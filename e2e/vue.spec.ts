import { test, expect } from '@playwright/test';

// See here how to get started:
// https://playwright.dev/docs/intro
test('visits the app root url', async ({ page }) => {
  await page.goto('/');
})


test('login', async ({ page }) => {
  await page.goto('/');
  await page.getByLabel("Din mail").fill('test@test.com');
  await page.getByLabel("Ditt lösenord").fill('Test123');
  await page.getByRole("button", { name: 'Skicka' }).click();
  // Wait for the button to appear on the page.
  await page.waitForSelector('button[type="button"].ternary');
  // Assert that the button exists.
  const button = await page.$('button[type="button"].ternary:text("Logga ut")');
  expect(button).toBeTruthy();
})
